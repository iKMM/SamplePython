# Welcome to the Github repository for Matt's Students.

If you're a student of mine, chances are you know what this repository is for. If you don't, you're probably in the wrong place!

You should find here the latest excerpts of different code we have put together either together or what I have produced independently outside of sessions.

Chances are you will find code on here that you do not recognise, as it is probably done in another language or done with another student.

Please download any code you wish and review it. I aim to include comments in all the code so that you can review fully what each bit does. If you have any questions, I'm happy for you to get in touch either through this site or MyTutor.

Thanks!

Matt