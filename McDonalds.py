def singleItem(): #Take note of camel case.
    print("Please enter your item:")
    mcDonaldsOrder.append(input())
    printOrder()

def meal():    
    print("Please enter your main meal item: (e.g. McNuggets, Burgers, etc.)")
    mcDonaldsOrder.append(input())
    print("Would you like small, medium or large fries?")
    mcDonaldsOrder.append(input() + " fries")
    print("What drink would you like, please specify the size also")
    mcDonaldsOrder.append(input())
    printOrder()

def printOrder():
    print("The order for customer %s:" % name)
    for x in mcDonaldsOrder:
        print(x)
    print("Please enter 'meal' or 'item' if you would like more, or type 'finished' to end order")

def finishOrder():
    print("The finsihed order for customer %s:" % name)
    for x in mcDonaldsOrder:
        print(x)
    print("Thank you for coming to McDonalds! Please come again soon and enjoy your meal!")
   
print("Hello, Welcome to McDonalds! What is your name?")
name = input()
print("Welcome, %s! Would you like a meal or a single item?" % name)


orderFinished = False

mcDonaldsOrder = [] 

while not orderFinished:
    decision = input().lower()
    if decision == "meal":
        meal()
    elif decision == "item":
        singleItem()
    elif decision == "finished":
        orderFinished = True
        finishOrder()
    else:
        print("Your decision was not understood, please try again! meal or single item?")
    
