def addTwoNumbers(number1, number2):
    return float(number1) + float(number2)

print("Hello! Please enter two numbers")

def printHello():
    print("Hello!")

a = input()
b = input()

answer = addTwoNumbers(a, b)

printHello()

print(answer)


#Simple function example for my students, adds two numbers and
#returns the value.

#Remember functions are written in the style: def functionName(parameters):

